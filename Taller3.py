# Genero listadelistas. Dado un .csv de n lineas y m columnas, genera una lista con n elementos. 
# Cada elemento de la listadelistas es una lista de m elementos.
import sys
import datetime

# Lee parámetros
parametros=sys.argv
ventana=int(parametros[3])
entradita=parametros[1]
salidita=parametros[2]


#Genera lista de listas con mediciones por sensor
def listadelistas(entradita):
    entrada = open(entradita)
    listadelistas=[]
    for i in entrada:
        listatemporal=(i.strip('\n').split(','))
        listadelistas.append(listatemporal)
    return listadelistas

listadelistas=listadelistas(entradita)
numerodemedidas=len(listadelistas)
numerodesensores=len(listadelistas[0])-1 #la primer columna es el tiempo


# Genero listadelistasprom, que contiene solo las medidas, promediando de a "ventana" medidas
def promedioventana(numerodemedidas,ventana,numerodesensores,listadelistas):
    listadelistasprom=[]
    for i in range(0,numerodemedidas-ventana+1):
        listatemporal=[]
        ti=datetime.datetime.strptime(listadelistas[i][0], '%Y-%m-%dT%H:%M:%S')
        tf=datetime.datetime.strptime(listadelistas[i+ventana-1][0], '%Y-%m-%dT%H:%M:%S')
        rango_de_tiempo=(tf - ti).total_seconds()
        listatemporal.append(str(rango_de_tiempo))

        for k in range(1,1+numerodesensores): 
            count = 0.0
            for j in range(i,i+ventana):
                if listadelistas[j][k] != "NA":               
                    count = count + (float((listadelistas[j][k]))/ventana) 
                else:                
                    count = "NA"
                    break
            if count == "NA":
                listatemporal.append(count)
            else:
                listatemporal.append("%.2f" % count)
        listadelistasprom.append(listatemporal)
    return listadelistasprom

listadelistasprom=promedioventana(numerodemedidas,ventana,numerodesensores,listadelistas)

# Para escribir la salida en la forma deseada
def escribirsalida(salidita,listadelistasprom):
    salida=open(salidita,'w')
    for i in range(0,len(listadelistasprom)):
        print(','.join(listadelistasprom[i]), file=salida)
    salida.close
    return

escribirsalida(salidita,listadelistasprom)
